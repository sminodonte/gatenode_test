// Example of communication between DAT (Data Aquisition & Trasformation) and GATE-NODE 

var socketio = require('socket.io-client')('http://127.0.0.1:3300');
// Example of payload, put here your data
// var payload = { "destination": ["viewer-raspberry"], "payload": {"source":"http://156.148.132.202/mjpg/video.mjpg", "angry": 18.2, "sad": 6.62, "neutral": 24.71, "disgust": 1.63, "scared": 1.86, "surprised": 0.5, "happy": 46.48}};
var payload = {"destination": ["raspberry_pi"], "payload": {"MESSAGE":"DATA FROM MAC_PAOLO", "posx":"1000", "posy":"900"}}
console.log(payload);
socketio.on('connect', () => {
  console.log('connect to server successful')
})

socketio.on('disconnect', () => {
  console.log('disconnect to server')
})
// Sent data from DAT (Data Aquisition & Trasformation) to GATE-NODE 
socketio.emit('gatenode', payload, e => {
  console.log('Sent data to GATE-NODE: ', e, payload);
})

// Received data from GATE-NODE to DAT    
socketio.on('gatenode', (data) => {
  console.log('Received data from GATE-NODE: ',data);
});

socketio.on('error', (e) => {
  console.log('Error: '+e)
})

socketio.on('connect_failed', () => {
   document.write("Sorry, there seems to be an issue with the connection!");
})