// Example of communication between DAT (Data Aquisition & Trasformation) and GATE-NODE 
let sequenceNumberByClient = new Map();

const socketio = require("socket.io").listen(3300);

// Event fired every time a new client connects:
socketio.on("connection", (socketio) => {
  console.info(`Client connected [id=${socketio.id}]`);
  // Initialize this client's sequence number
  sequenceNumberByClient.set(socketio, 1);
  var message; 

    // var payload = { "destination": ["viewer-raspberry"], "payload": {"source":"http://156.148.132.202/mjpg/video.mjpg", "angry": 18.2, "sad": 6.62, "neutral": 24.71, "disgust": 1.63, "scared": 1.86, "surprised": 0.5, "happy": 46.48}};
    // console.log(payload);

    // Sent data from GATE-NODE to DAT (Data Aquisition & Trasformation)

    // socketio.emit('gatenode', payload, e => {
    //   console.log('Sent data from DAT to GATE-NODE: ', e);
    // })
 
  // Received data from DAT (Data Aquisition & Trasformation) to GATE-NODE     
  socketio.on('gatenode', function (data) {
    console.log('Received data from DAT to GATE-NODE: ',data);

    // FORMATO DEL MESSAGGIO JSON:
    // data = { "destination": [node1,node2,node3,...], payload:{}}

    // QUI BISOGNA FILTRARE IL NOME DEL NODO A CUI VOGLIAMO INVIARE LE INFORMAZIONI
    // POTREMMO FARE UNA RICERCA IN BASE AL NOME DEL NODO E PRELEVARE L'UUID CHE SERVE PER 
    // INVIARE UN MESSAGGIO AD UN SPECIFICO NODO
  
              
  });

    socketio.on('gatenode_p2p', function (data) {
    console.log('Received data from DAT to GATE-NODE: ',data);

    // FORMATO DEL MESSAGGIO JSON:
    // data = { "destination": [node1,node2,node3,...], payload:{}}

    // QUI BISOGNA FILTRARE IL NOME DEL NODO A CUI VOGLIAMO INVIARE LE INFORMAZIONI
    // POTREMMO FARE UNA RICERCA IN BASE AL NOME DEL NODO E PRELEVARE L'UUID CHE SERVE PER 
    // INVIARE UN MESSAGGIO AD UN SPECIFICO NODO
  
              
  });

  // When socket disconnects, remove it from the list:
  socketio.on("disconnect", () => {
      sequenceNumberByClient.delete(socketio);
      console.info(`Client gone [id=${socketio.id}]`);
  });


    // var pathImage = message.payload.url;
    // document.getElementById('image').src = pathImage;
    
});