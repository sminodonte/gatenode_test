// Example of communication between DAT (Data Aquisition & Trasformation) and GATE-NODE 

var socketio = require('socket.io-client')('http://127.0.0.1:3200');
var d = new Date();
var sending_time = d.getTime();

// Example of data to send

// var data  = {
//   "sender": "node0",
//   "receiver": {
//     "name": ["node1"],
//     "type": "compute", // [stream, compute, collect, view]
//     "query": "name" // [name, type]
//   },
//   "data": {
//     "type": "message",
//     "payload": {"data":"message from node0"}
//   }
// }

var data ={
  "sender": "node1",
  "receiver": {
    "name": "sauron" // Only one node because is peer-to-peer communication
  },
  "data": {
    "protocol": "udp", // [udp, zeromq, osc, ...]
    "role": "sender",  // [sender, receiver, *]
    "payload": {"algorithm":"tracked_objects", "output":"data sent from node0" } // This Object is the algorithm's output
  }
}

console.log(data);
socketio.on('connect', () => {
  console.log('Connect to GATE-NODE successful')
})

socketio.on('disconnect', () => {
  console.log('disconnect to GATE-NODE')
})
// Sent data from DAT (Data Aquisition & Trasformation) to GATE-NODE 
socketio.emit('gatenode', data, e => {
  console.log('Sent data to GATE-NODE: ', e, data);
})

// Received data from GATE-NODE to DAT    
socketio.on('gatenode', (data) => {
  console.log('Received data from GATE-NODE: ',data);
});

// Sent data from DAT (Data Aquisition & Trasformation) to GATE-NODE-P2P
socketio.emit('gatenode_p2p', data, e => {
  console.log('Sent data to GATE-NODE-P2P: ', e, data);
})

// Received data from GATE-NODE-P2P to DAT    
socketio.on('gatenode_p2p', (data) => {
  console.log('Received data from GATE-NODE-P2P: ',data);
});

socketio.on('error', (e) => {
  console.log('Error: '+e)
})

socketio.on('connect_failed', () => {
   document.write("Sorry, there seems to be an issue with the connection!");
})