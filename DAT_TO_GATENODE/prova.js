// Example of communication between DAT (Data Aquisition & Trasformation) and GATE-NODE 

var socketio = require('socket.io-client')('http://0.0.0.0:3200');
// EXAMPLE DATA FOR SAURON

var data  = {
  "sender": "paolo",
  "receiver": {
    "name": ["sauron"],
    "type": "compute", // [stream, compute, collect, view]
    "query": "name" // [name, type]
  },
  "data": {
    "type": "message",
    "payload": {"data":"message from paolo"}
  }
}

var data_p2p ={
  "sender": "paolo",
  "receiver": {
    "name": "sauron" // Only one node because is peer-to-peer communication
  },
  "data": {
    "protocol": "udp", // [udp, zeromq, osc, ...]
    "role": "sender",  // [sender, receiver, *]
    "payload": {"algorithm":"crowd_counting", "output":"data sent from paolo" } // This Object is the algorithm's output
  }
}

console.log(data);
console.log(data_p2p);
socketio.on('connect', () => {
  console.log('Connect to GATE-NODE successful')
    socketio.emit('gatenode', data);
  socketio.emit('gatenode_p2p', data_p2p);

})

setInterval(function(){
  // Sent data from DAT (Data Aquisition & Trasformation) to GATE-NODE-P2P
  socketio.emit('gatenode', data);
  socketio.emit('gatenode_p2p', data_p2p);
}, 1000);
// Received data from GATE-NODE-P2P to DAT    
socketio.on('gatenode_p2p', (data) => {
  console.log('Received data from GATE-NODE-P2P: ',data);
});
