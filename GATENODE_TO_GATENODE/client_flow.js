const socket = require(`zeromq`).socket(`req`);     // REQuest socket

socket.connect(`udp://127.0.0.1:3000`);          // Same address

var counter = 0;                                 // Message counter

socket.on(`message`, function (msg) {            // When receive message
	console.log(`Response received: "${msg}"`);
	setTimeout(sendMessage, 2000);               // Schedule next request
});

//EVENT HANDLER gatenode_data: ->  for stream
var msg  = {
  "sender": "node0",
  "receiver": {
    "name": "sauron_api" // Only one node because is peer-to-peer communication
  },
  "data": {
    "protocol": "zeromq", // [zeromq, osc, ...]
    "role": "sender",  // [sender, receiver, *]
    "payload": {"algorithm":"counting", "payload": ""}
  }
}

sendMessage();

function sendMessage () {
	//const msg = `MSG FROM FLOW#${counter++}`;
    
	console.log(`Sending ${msg}`);
	socket.send(JSON.stringify(msg));                            // Send request
}