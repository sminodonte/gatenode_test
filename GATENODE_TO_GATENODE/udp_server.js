var PORT = 33333;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var server = dgram.createSocket('udp4');

server.on('listening', function() {
  var address = server.address();
 console.log('UDP Server listening on ' + address.address + ':' + address.port);
});
var counter = 0;
server.on('message', function(message, remote) {
	counter++;	
	if(JSON.parse(message).data.payload.algorithm==="counting"){
	   console.log("Algorithm counting people")
 	   console.log('['+counter+'] '+remote.address + ':' + remote.port +' - ' + message);
	}
	if(JSON.parse(message).data.payload.algorithm==="flow"){
	   console.log("Algorithm flow")
	   console.log('['+counter+'] '+remote.address + ':' + remote.port +' - ' + message);
	}
});

server.bind(PORT, HOST);


// const dgram = require('dgram');
// const server = dgram.createSocket('udp4');

// server.on('error', (err) => {
//   console.log(`server error:\n${err.stack}`);
//   server.close();
// });

// server.on('message', (message, rinfo) => {
//   console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
// });

// server.on('listening', () => {
//   const address = server.address();
//   console.log(`server listening ${address.address}:${address.port}`);
// });

// server.bind(41234);
// // server listening 0.0.0.0:41234