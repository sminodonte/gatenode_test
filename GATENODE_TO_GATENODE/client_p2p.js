//Peer Client code
const zeromq = require(`zeromq`).socket(`pair`);
const address = `tcp://127.0.0.1:3332`;
console.log(`Connecting to ${address}`);
zeromq.connect(address);

zeromq.on(`message`, function (msg) {
    console.log(`Message received: ${msg}`);
});

const message = `Ping from client zeroMQ`;

const sendMessage = function () {
	console.log("Sending message to server zeroMQ")
    zeromq.send(message);
};
setInterval(sendMessage, 1000);

//console.log(getIPAddresses());

// setInterval(sendMessage2, 30000);

// function getIPAddresses() {
//     var os = require("os"),
//     interfaces = os.networkInterfaces(),
//     ipAddresses = [];
    
//     for (var deviceName in interfaces) {
//       var addresses = interfaces[deviceName];
//       for (var i = 0; i < addresses.length; i++) {
//         var addressInfo = addresses[i];
//         if (addressInfo.family === "IPv4" && !addressInfo.internal) {
//             ipAddresses.push(addressInfo.address);
//         }
//       }
//     }
//     return ipAddresses;
// } 
