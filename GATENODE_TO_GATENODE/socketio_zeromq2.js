// Example of communication between GATE-NODE to GATE-NODE 
// 1) handshake along socketio for create communication
// 2) start sending stream along zeromq
const zeromq = require(`zeromq`).socket(`pair`);

let sequenceNumberByClient = new Map();

const socketio = require("socket.io").listen(3300);

var data  = {
  "sender": "node0",
  "topic": "TEST", // [handshake,message,stream]
  "receiver": {
    "name": ["nodeA","nodeB","nodeB","nodeD"],
    "type": "compute", // [stream, compute, collect, view]
    "query": "name" // [name, type]
  },
  "data": {
    "message": {
      "payload": {"online":"message"}
    },
    "stream": {       
      "protocol": "zeromq", // [zeromq, osc, ...]
      "role_receiver": "server",  // [server, client]
      "address": "tcp://127.0.0.1:3000",      
      "payload": {"online":"stream"}
    }
  }
}

socketio.on("connection", (socketio) => {
  console.info(`Client connected [id=${socketio.id}]`);
  // Initialize this client's sequence number
  sequenceNumberByClient.set(socketio, 1);
 
   //setInterval(function(){
       socketio.emit('message', data)
   //},2000);

  // Received data from DAT (Data Aquisition & Trasformation) to GATE-NODE     
  socketio.on('message', function (data) {
    console.log('2° Received data from DAT to MIDDLE-NODE: ',data);

    // IF THIS NODE IS CLIENT
  if(data.data.stream.role_receiver==="client"){
    console.log("CLIENT");
    //Peer Client code
    const address = data.data.stream.address;
    console.log(`Connecting to ZEROMQ: ${address}`);
    zeromq.connect(address);

    zeromq.on(`message`, function (msg) {
      console.log(`Message received: ${msg}`);
    });

    const sendMessage2 = function () {
      const message = `Ping 2`;
      console.log(`Sending 2 '${message}'`);
      zeromq.send(message);
    };
  }
  // IF THIS NODE IS SERVER
  if(data.data.stream.role_receiver==="server"){
    console.log("SERVER");
    //Peer Server Code
    const address = `tcp://127.0.0.1:3000`;  
    console.log(`Listening at ${address}`);
    zeromq.bindSync(address);

    zeromq.on(`message`, function (msg) {
        console.log(`Message received 2: ${msg}`);
    });

    const sendMessage = function () {
        const message = `Ping 1`;
        console.log(`Sending 1 '${message}'`);
        zeromq.send(message);
    };
    setInterval(sendMessage, 1000);
  }
            
  });

  // When socket disconnects, remove it from the list:
  socketio.on("disconnect", () => {
      sequenceNumberByClient.delete(socketio);
      console.info(`Client gone [id=${socketio.id}]`);
  });


    
});