var PORT = 3333;
var HOST = '10.147.18.139';

const dgram = require('dgram');
//EVENT HANDLER gatenode_data: ->  for stream
var msg  = {
  "sender": "node0",
  "receiver": {
    "name": "API" // Only one node because is peer-to-peer communication
  },
  "data": {
    "protocol": "udp", // [zeromq, osc, ...]
    "role": "sender",  // [sender, receiver, *]
    "payload": {"algorithm":"counting", "payload": ""}
  }
}

const message = Buffer.from(JSON.stringify(msg));
const client = dgram.createSocket('udp4');


setInterval(function(){
  client.send(message, PORT, HOST, (err) => {
	console.log('UDP message sent to ' + HOST +':'+ PORT);
  //	client.close();
});
}, 500);