// Example of communication between GATE-NODE to GATE-NODE 
// 1) handshake along socketio for create communication
// 2) start sending stream along zeromq

var socketio = require('socket.io-client')('http://127.0.0.1:3300');
const zeromq = require(`zeromq`).socket(`pair`);

// Example of data, put here your data
// var data = { "destination": ["viewer-raspberry"], "payload": {"source":"http://156.148.132.202/mjpg/video.mjpg", "angry": 18.2, "sad": 6.62, "neutral": 24.71, "disgust": 1.63, "scared": 1.86, "surprised": 0.5, "happy": 46.48}};
var data  = {
  "sender": "node0",
  "topic": "TEST", // [handshake,message,stream]
  "receiver": {
    "name": ["nodeA","nodeB","nodeB","nodeD"],
    "type": "compute", // [stream, compute, collect, view]
    "query": "name" // [name, type]
  },
  "data": {
    "message": {
      "payload": {"online":"message"}
    },
    "stream": {       
      "protocol": "zeromq", // [zeromq, osc, ...]
      "role_receiver": "server",  // [server, client]
      "address": "tcp://127.0.0.1:3000",      
      "payload": {"online":"stream"}
    }
  }
}


socketio.on('connect', () => {
  console.log('connect to server successful')
})

socketio.on('disconnect', () => {
  console.log('disconnect to server')
})
// Sent data from DAT (Data Aquisition & Trasformation) to GATE-NODE 
socketio.emit('message', data, e => {
  console.log(' 1° Sent data to MIDDLE-NODE: ', e, data);
})

// Received data from GATE-NODE to DAT    
socketio.on('message', (data) => {
  console.log('Received data from MIDDLE-NODE: ',data);

  // IF THIS NODE IS CLIENT
  if(data.data.stream.role_receiver==="client"){
    console.log("CLIENT");

    //Peer Client code
    const address = data.data.stream.address;
    console.log(`Connecting to ZEROMQ: ${address}`);
    zeromq.connect(address);

    zeromq.on(`message`, function (msg) {
      console.log(`Message received: ${msg}`);
    });

    const sendMessage2 = function () {
      const message = `Ping 2`;
      console.log(`Sending 2 '${message}'`);
      zeromq.send(message);
    };
  }
  // IF THIS NODE IS SERVER
  if(data.data.stream.role_receiver==="server"){
    console.log("SERVER");
    //Peer Server Code
    const address = `tcp://127.0.0.1:3000`;  
    console.log(`Listening at ${address}`);
    zeromq.bindSync(address);

    zeromq.on(`message`, function (msg) {
        console.log(`Message received 2: ${msg}`);
    });

    const sendMessage = function () {
        const message = `Ping 1`;
        console.log(`Sending 1 '${message}'`);
        zeromq.send(message);
    };
    setInterval(sendMessage, 1000);
  }



});

socketio.on('error', (e) => {
  console.log('Error: '+e)
})

socketio.on('connect_failed', () => {
   console.log("Sorry, there seems to be an issue with the connection!");
})
