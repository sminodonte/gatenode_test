//Peer Server Code
const zeromq = require(`zeromq`).socket(`pair`);
const address = 'tcp://0.0.0.0:3332';  
console.log(`Listening at ${address}`);
zeromq.bindSync(address);

zeromq.on(`message`, function (msg) {
    console.log(`Message received 2: ${msg}`);
});

const sendMessage = function () {
    const message = `Sending ping from zeroMQ server`;
    console.log('Sending ping from zeroMQ server OUTSIDE CONTAINER');
    zeromq.send(message);
};
setInterval(sendMessage, 2000);


// const sendMessage = function () {
//     console.log(`Sending 1 '${message}'`);
//     zeromq.send(message);
// };
// setInterval(sendMessage, 1000);

