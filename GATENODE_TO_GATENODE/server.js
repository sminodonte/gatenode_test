
const socket = require(`zeromq`).socket(`rep`);  // REsPonse socket 
 
socket.bindSync(`udp://127.0.0.1:3000`);      // listening at ..1:3000
 
socket.on(`message`, function (msg) {         // on message
    console.log(`Received '${msg}'. Responding...`);
    socket.send(`Responding to ${msg}`);      // send something back

    //console.log(msg)
    // if(JSON.parse(msg).data.payload.algorithm==="counting"){
    //    console.log("Algorithm counting people")
    // }
    // if(JSON.parse(msg).data.payload.algorithm==="flow"){
    //    console.log("Algorithm flow")
    // }
});